﻿(function () {
    $(document).ready(function () {
        ipadrotation();
        breakout();
        setbookmark();
        loadCSS(qualifyUrl("Content/fontloader.css"));
        loadindexheading();
        $.cookieBar();
        $('#BibleBooks').change(function () {
            showProgress();
            var bookno = $("#BibleBooks").val();
            switch (bookno) {
                case "0":
                    window.location.href = qualifyUrl("");
                    break;
                case "100":
                    window.location.href = qualifyUrl("Parallel/1/1");
                    break;
                case "200":
                    window.location.href = qualifyUrl("Bible/1/1");
                    break;
                default:
                    window.location.href = qualifyUrl("Bible/" + bookno + "/1");
            }
        });
        $('#BibleBooksParallel').change(function () {
            showProgress();
            var bookno = $("#BibleBooksParallel").val();
            switch (bookno) {
                case "0":
                    window.location.href = qualifyUrl("");
                    break;
                case "100":
                    window.location.href = qualifyUrl("Parallel/1/1");
                    break;
                case "200":
                    window.location.href = qualifyUrl("Bible/1/1");
                    break;
                default:
                    window.location.href = qualifyUrl("Bible/" + bookno + "/1");
            }
        });
        $('.bookref').click(function () {
            showProgress();
            var bookno = $(this).attr("data-book");
            if ($('#ParallelBible').length) {
                window.location.href = qualifyUrl("Parallel/" + bookno + "/1");
            }
            else {
                window.location.href = qualifyUrl("Bible/" + bookno + "/1");
            }
        });
        $('.navhead a').click(function () {
            showProgress();
        });
        $('.chapterref a').click(function () {
            showProgress();
        });
        $('.index-body-content a').click(function () {
            showProgress();
        });
        $('.navbtmbtn a').click(function () {
            showProgress();
        });
        $('.marginnoteimg').click(function (event) {
            showmarginnote(event);
        });
        $('.marginnoteimg').hover(imageSwap, imageSwap);
        $('.frontpage .bs-callout a').click(function () {
            showProgress();
        });
        $(".aniload").click(function () {
            showProgress();
            return true;
        });
        $(".link-callout").hover(function () {
            $(".link-callout").css('cursor', 'pointer');
        });
        $(".link-callout").click(function (event) {
            linkcallout(event);
        });
    });
    var setbookmark = function () {
        var bookno = $("#BookNo").val();
        var chapterno = $("#ChapterNo").val();
        if ($("#BookNo").length) {
            $('.bookref').each(function () {
                if ($(this).attr("data-book") === bookno) {
                    $(this).css("color", "#fff");
                    $(this).css("background-color", "#464646");
                    return false;
                }
            });
            $('.chapterref a').each(function () {
                if ($(this).attr("data-chapter") === chapterno) {
                    $(this).css("color", "#fff");
                    $(this).css("background-color", "#464646");
                    return false;
                }
            });
        }
        if ($("#BibleBooks").length) {
            $("#BibleBooks").val(bookno);
        }
        else {
            $("#BibleBooks").val(0);
        }
        if ($("#BibleBooksParallel").length) {
            $("#BibleBooksParallel").val(bookno);
        }
    };
    var breakout = function () {
        if (top.location !== location) {
            top.location.href = document.location.href;
        }
    };
    var ipadrotation = function () {
        if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
            var viewportmeta = document.querySelector('meta[name="viewport"]');
            if (viewportmeta) {
                viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
                if (document.addEventListener) {
                    document.addEventListener('gesturestart', function () {
                        viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=10';
                    }, false);
                }
            }
        }
    };
    var qualifyUrl = function (pathinfo) {
        var qualifiedUrl;
        var pathArray = window.location.href.split('/');
        var protocol = pathArray[0];
        var host = pathArray[2];
        var site = pathArray[3];
        if (host.indexOf('localhost:') > -1) {
            qualifiedUrl = protocol + '//' + host + '/' + pathinfo;
        }
        else if (host.indexOf('localhost') > -1 || host.indexOf('192.168') > -1) {
            qualifiedUrl = protocol + '//' + host + '/' + site + '/' + pathinfo;
        }
        else {
            qualifiedUrl = protocol + '//' + host + '/' + pathinfo;
        }
        return qualifiedUrl;
    };
    var loadindexheading = function () {
        $("#IndexHeading").html("<h2 class='kjvindexheading'>Authorised Version</h2>");
    };
    var showProgress = function () {
        clearTimeout(hideProgress);
        var spinner = $("div#spinner");
        if (spinner.css('display') === 'none') {
            spinner.html('');
            spinner.append("<img src='" + qualifyUrl("Content/images/loader.gif") + "' /><br /><p>Loading...</p>");
            spinner.fadeIn("fast");
            setTimeout(hideProgress, 10000);
        }
    };
    var hideProgress = function () {
        var spinner = $("div#spinner");
        if (spinner.css('display') !== 'none') {
            spinner.fadeOut("fast");
        }
    };
    var linkcallout = function (event) {
        var dataaction = $(event.currentTarget).attr('data-action');
        if (dataaction !== null) {
            showProgress();
            window.location.href = qualifyUrl(dataaction);
        }
    };
    var showmarginnote = function (event) {
        hidemarginnote();
        var bibleid = $(event.target).attr('data-bibleid');
        if (bibleid !== null) {
            $.getJSON({
                url: qualifyUrl("GetMarginNote/" + bibleid),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('<div class="marginnote"><div class="closenote">X</div>' + data + '</div>').appendTo('body');
                    $(".closenote").bind("click", function () { hidemarginnote(); });
                    $(".marginnote").center();
                },
                error: function (xhr) {
                    alert('Server Error!');
                }
            });
        }
    };
    var hidemarginnote = function () {
        $(".marginnote").stop(true, true).remove();
    };
    var imageSwap = function () {
        var $this = $(this);
        var newSource = $this.attr('data-alt-src');
        if (newSource.indexOf("~/") >= 0) {
            newSource = newSource.replace("~/", "");
            newSource = qualifyUrl(newSource);
        }
        $this.attr('data-alt-src', $this.attr('src'));
        $this.attr('src', newSource);
    };
    var createCookie = function (name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
    };
    var readCookie = function (name) {
        var nameEQ = encodeURIComponent(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0)
                return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
        return null;
    };
    var eraseCookie = function (name) {
        createCookie(name, "", -1);
    };
    $.fn.preload = function () {
        this.each(function () {
            $('<img/>')[0].src = this;
        });
    };
    $.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", Math.max(0, ($(window).height() - $(this).outerHeight()) / 2 +
            $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, ($(window).width() - $(this).outerWidth()) / 2 +
            $(window).scrollLeft()) + "px");
        return this;
        //// sample call: $(element).center();
    };
}());
