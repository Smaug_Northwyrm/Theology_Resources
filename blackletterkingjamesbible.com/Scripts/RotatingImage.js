﻿$(document).ready(function () {
    $(".rotating-img").click(function () {
        var doc = $(this).attr("data-ref");
        if (doc.length) {
            window.open(doc, "_blank");
        }
    });
    var InfiniteRotator = {
        init: function () {
            var initialFadeIn = 1000;
            var itemInterval = 5000;
            var fadeTime = 2500;
            var numberOfItems = $(".rotating-img").length;
            var currentItem = 0;
            $(".rotating-img").eq(currentItem).fadeIn(initialFadeIn);
            var infiniteLoop = setInterval(function () {
                $(".rotating-img").eq(currentItem).fadeOut(fadeTime);
                if (currentItem === numberOfItems - 1) { currentItem = 0; }
                else { currentItem++; }
                $(".rotating-img").eq(currentItem).fadeIn(fadeTime);
            }, itemInterval);
        }
    };
    InfiniteRotator.init();
});