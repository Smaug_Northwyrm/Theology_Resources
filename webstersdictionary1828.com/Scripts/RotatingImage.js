﻿$(document).ready(function () {
    $(".rotating-img").click(function () {
        var doc = $(this).attr("data-ref");
        if (doc.length) {
            window.open(doc, "_blank");
        }
    });
    var InfiniteRotator = {
        init: function () {
            var initialFadeIn = 400;
            var itemInterval = 9000;
            var fadeTime = 600;
            var numberOfItems = $(".rotating-img").length;
            var currentItem = Math.floor(Math.random() * numberOfItems);
            $(".rotating-img").eq(currentItem).fadeIn(initialFadeIn);
            setInterval(function () {
                $(".rotating-img").eq(currentItem).fadeOut(fadeTime);
                if (currentItem === numberOfItems - 1) { currentItem = 0; }
                else { currentItem++; }
                $(".rotating-img").eq(currentItem).fadeIn(fadeTime);
            }, itemInterval);
        }
    };
    InfiniteRotator.init();
});