﻿$(document).ready(function () {
    $(".advert-img").click(function () {
        var doc = $(this).attr("data-ref");
        if (doc.length) {
            window.open(doc, "_blank");
        }
    });
    var InfiniteRotator = {
        init: function () {
            var itemInterval = 5000;
            var fadeTime = 1500;
            var numberOfItems = $(".advert-img").length;
            var currentItem = 0;
            $(".advert-img").eq(currentItem).fadeIn(fadeTime);
            var infiniteLoop = setInterval(function () {
                $(".advert-img").eq(currentItem).fadeOut(fadeTime);
                if (currentItem === numberOfItems - 1) { currentItem = 0; }
                else { currentItem++; }
                $(".advert-img").eq(currentItem).fadeIn(fadeTime);
            }, itemInterval);
        }
    };
    InfiniteRotator.init();
});