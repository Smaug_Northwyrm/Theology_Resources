﻿(function () {
    $(document).ready(function () {
        getAtlasId();
    });
    var getAtlasId = function () {
        var atlasid = $("#map").attr("data-atlasid");
        if (atlasid > 0) { mapInit(atlasid); }
    };
    var mapInit = function (atlasid) {
        try {
            $.ajax({
                type: "POST",
                url: qualurl("Atlas/" + atlasid),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    load_map(data);
                },
                error: function (request, status, error) {
                    alert('Error! ' + error);
                }
            });
        }
        catch (err) {
            alert('Map Initialization Error! ' + err);
        }
    };

    var exlzone = [];
    var load_map = function (data) {
        window.place = data[0].PlaceName;
        window.lat = parseFloat(data[0].Latitude);
        window.lon = parseFloat(data[0].Longitude);
        window.zm = parseInt('9');
        window.extent = ol.extent.createEmpty();
        window.markerVectorLayer = null;
        window.map = null;
        map = new ol.Map({
            target: 'map',
            layers: [new ol.layer.Tile({
                source: new ol.source.OSM(),
                opacity: 1.0,
                brightness: 1.0
            })],
            view: new ol.View({
                center: ol.proj.fromLonLat([lon, lat]),
                zoom: zm,
                minZoom: 2,
                maxZoom: 16
            })
        });
        setPrimeMarker(data);
        if (objSize(data) > 1) {
            map.getView().fit(extent, map.getSize());
        }
        setCountryMarkers();
        setCityMarkers();
    };
    var Inclusion = function (xlat, xlng) {
        var isIncluded = true;
        for (let i = 0; i < exlzone.length; i++) {
            if (getDistance(xlat, xlng, exlzone[i].Latitude, exlzone[i].Longitude) < 12) { isIncluded = false; }
        }
        if (isIncluded) { exlzone.push({ Latitude: xlat, Longitude: xlng }); }
        return isIncluded;
    };
    var setPrimeMarker = function (data) {
        data.forEach(parseAtlasItem);
    };
    var parseAtlasItem = function (val, i) {
        var img = qualurl("Content/images/marker.png");
        place = val.PlaceName;
        lat = parseFloat(val.Latitude);
        lon = parseFloat(val.Longitude);
        if (Inclusion(lat, lon)) {
            var marker = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'))
            });
            marker.setStyle(new ol.style.Style({
                image: new ol.style.Icon({
                    anchor: [16, 16],
                    anchorYUnits: 'pixels',
                    anchorXUnits: 'pixels',
                    opacity: 1.0,
                    src: img
                }),
                text: new ol.style.Text({
                    text: place,
                    font: 'bold 15pt Arial',
                    fill: new ol.style.Fill({ color: '#333366' }),
                    stroke: new ol.style.Stroke({ color: '#ffffff', width: 3 }),
                    textAlign: 'center',
                    textBaseline: 'middle',
                    offsetX: parseInt(0, 0),
                    offsetY: parseInt(-25, 0),
                    rotation: 0,
                    placement: 'point',
                    maxAngle: 0.7853981633974483
                })
            }));
            var vectorSource = new ol.source.Vector({
                features: [marker]
            });
            markerVectorLayer = new ol.layer.Vector({
                source: vectorSource
            });
            markerVectorLayer.setZIndex(80);
            ol.extent.extend(extent, markerVectorLayer.getSource().getExtent());
            map.addLayer(markerVectorLayer);
        }
    };
    var setCountryMarkers = function () {
        var countryobj = [
            { PlaceName: 'ISRAEL', Latitude: 31.013688, Longitude: 34.852722 },
            { PlaceName: 'JORDAN', Latitude: 31.283897, Longitude: 36.833414 },
            { PlaceName: 'LEBANON', Latitude: 34.154750, Longitude: 35.902735 },
            { PlaceName: 'SYRIA', Latitude: 35.046562, Longitude: 38.501018 },
            { PlaceName: 'MESOPOTAMIA', Latitude: 33.066698, Longitude: 42.951147 },
            { PlaceName: 'IRAN', Latitude: 32.289095, Longitude: 53.776427 },
            { PlaceName: 'ARABIA', Latitude: 29.124294, Longitude: 40.757078 },
            { PlaceName: 'EGYPT', Latitude: 30.657311, Longitude: 31.038268 },
            { PlaceName: 'CYPRUS', Latitude: 35.015304, Longitude: 33.211548 },
            { PlaceName: 'ASIA-MINOR', Latitude: 39.060162, Longitude: 35.365907 },
            { PlaceName: 'GREECE', Latitude: 39.516763, Longitude: 21.710876 },
            { PlaceName: 'ITALY', Latitude: 41.564488, Longitude: 14.330875 },
            { PlaceName: 'CANAAN', Latitude: 31.693530, Longitude: 34.843883 },
            { PlaceName: 'WEST BANK', Latitude: 32.099667, Longitude: 35.250014 },
            { PlaceName: 'PERSIA', Latitude: 34.798311, Longitude: 48.514966 },
            { PlaceName: 'ASSYRIA', Latitude: 36.639410, Longitude: 43.152887 },
            { PlaceName: 'GALILEE', Latitude: 32.884627, Longitude: 35.295889 },
            { PlaceName: 'GAZA', Latitude: 31.484319, Longitude: 34.427114 },
            { PlaceName: 'IDUMEA', Latitude: 30.734691, Longitude: 35.606250 },
            { PlaceName: 'DECAPOLIS', Latitude: 32.475410, Longitude: 35.976593 },
            { PlaceName: 'PEREA', Latitude: 32.132270, Longitude: 35.755413 },
            { PlaceName: 'RED SEA', Latitude: 27.088473, Longitude: 34.771729 },
            { PlaceName: 'CRETE', Latitude: 35.171239, Longitude: 25.007204 },
            { PlaceName: 'MELITA', Latitude: 35.852827, Longitude: 14.532436 },
            { PlaceName: 'MACEDONIA', Latitude: 41.512975, Longitude: 23.084444 },
            { PlaceName: 'SPAIN', Latitude: 40.250000, Longitude: -3.450000 },
            { PlaceName: 'CHALDEA', Latitude: 30.802052, Longitude: 46.003742 },
            { PlaceName: 'GALATIA', Latitude: 37.578135, Longitude: 32.453183 }
        ];
        var countriesjson = JSON.stringify(countryobj);
        var countries = JSON.parse(countriesjson);
        countries.forEach(parseCountryItem);
        countries.forEach(parseCountryItem);
    };
    var parseCountryItem = function (val, i) {
        var img = qualurl("Content/images/invisiblemarker.png");
        place = val.PlaceName;
        lat = parseFloat(val.Latitude);
        lon = parseFloat(val.Longitude);
        if (Inclusion(lat, lon)) {
            var marker = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'))
            });
            marker.setStyle(new ol.style.Style({
                image: new ol.style.Icon({
                    anchor: [4, 4],
                    anchorYUnits: 'pixels',
                    anchorXUnits: 'pixels',
                    opacity: 1.0,
                    src: img
                }),
                text: new ol.style.Text({
                    text: place,
                    font: 'bold 14pt Arial',
                    fill: new ol.style.Fill({ color: '#cc66cc' }),
                    textAlign: 'center',
                    textBaseline: 'middle',
                    offsetX: parseInt(0, 0),
                    offsetY: parseInt(0, 0),
                    rotation: 0,
                    placement: 'point',
                    maxAngle: 0.7853981633974483
                })
            }));
            var vectorSource = new ol.source.Vector({
                features: [marker]
            });
            markerVectorLayer = new ol.layer.Vector({
                source: vectorSource
            });
            markerVectorLayer.setZIndex(10);
            ol.extent.extend(extent, markerVectorLayer.getSource().getExtent());
            map.addLayer(markerVectorLayer);
        }
    };
    var setCityMarkers = function () {
        var cityobj = [
            { PlaceName: 'Jerusalem', Latitude: 31.781672, Longitude: 35.205876 },
            { PlaceName: 'Joppa', Latitude: 32.053520, Longitude: 34.750426 },
            { PlaceName: 'Damascus', Latitude: 33.520658, Longitude: 36.266749 },
            { PlaceName: 'Ammon', Latitude: 31.943979, Longitude: 35.923619 },
            { PlaceName: 'Beirut', Latitude: 33.897517, Longitude: 35.482030 },
            { PlaceName: 'Aleppo', Latitude: 36.203153, Longitude: 37.123901 },
            { PlaceName: 'Baghdad', Latitude: 33.329213, Longitude: 44.357785 },
            { PlaceName: 'Tehran', Latitude: 35.681073, Longitude: 51.381442 },
            { PlaceName: 'Riyadh', Latitude: 24.718466, Longitude: 46.653278 },
            { PlaceName: 'Cairo', Latitude: 30.046509, Longitude: 31.232461 },
            { PlaceName: 'Alexandria', Latitude: 31.199394, Longitude: 29.919443 },
            { PlaceName: 'Nicosia', Latitude: 35.187484, Longitude: 33.378294 },
            { PlaceName: 'Istanbul', Latitude: 41.015624, Longitude: 28.967809 },
            { PlaceName: 'Antioch', Latitude: 36.210087, Longitude: 36.160279 },
            { PlaceName: 'Athens', Latitude: 37.981865, Longitude: 23.724619 },
            { PlaceName: 'Babylon', Latitude: 32.536504, Longitude: 44.420883 },
            { PlaceName: 'Jordan River', Latitude: 32.309099, Longitude: 35.559900 },
            { PlaceName: 'Moab', Latitude: 31.496845, Longitude: 35.782841 },
            { PlaceName: 'Gilead', Latitude: 32.042523, Longitude: 35.724241 },
            { PlaceName: 'Edom', Latitude: 30.734691, Longitude: 35.606250 },
            { PlaceName: 'Hebron', Latitude: 31.535774, Longitude: 35.094100 },
            { PlaceName: 'Dan', Latitude: 33.248660, Longitude: 35.652483 },
            { PlaceName: 'Jericho', Latitude: 31.870601, Longitude: 35.443864 },
            { PlaceName: 'Shechem', Latitude: 32.213691, Longitude: 35.281799 },
            { PlaceName: 'Nazareth', Latitude: 32.706745, Longitude: 35.301528 },
            { PlaceName: 'Kadesh', Latitude: 30.687713, Longitude: 34.494796 },
            { PlaceName: 'Sinai', Latitude: 28.539722, Longitude: 33.973333 },
            { PlaceName: 'Samaria', Latitude: 32.280231, Longitude: 35.197929 },
            { PlaceName: 'Tyre', Latitude: 33.275828, Longitude: 35.192575 },
            { PlaceName: 'Mount Carmel', Latitude: 32.729350, Longitude: 35.049790 },
            { PlaceName: 'Ashkelon', Latitude: 31.662405, Longitude: 34.547228 },
            { PlaceName: 'Beersheba', Latitude: 31.244952, Longitude: 34.840889 },
            { PlaceName: 'Rameses', Latitude: 30.799370, Longitude: 31.834217 },
            { PlaceName: 'Patmos', Latitude: 37.307520, Longitude: 26.548274 },
            { PlaceName: 'Ephesus', Latitude: 37.95253, Longitude: 27.367825 },
            { PlaceName: 'Laodicea', Latitude: 37.769867, Longitude: 29.064501 },
            { PlaceName: 'Pergamos', Latitude: 39.118946, Longitude: 27.165126 },
            { PlaceName: 'Philadelphia', Latitude: 38.349048, Longitude: 28.519462 },
            { PlaceName: 'Sardis', Latitude: 38.476827, Longitude: 28.114131 },
            { PlaceName: 'Smyrna', Latitude: 38.451960, Longitude: 27.161921 },
            { PlaceName: 'Thyatira', Latitude: 38.925791, Longitude: 27.856556 },
            { PlaceName: 'Ephraim', Latitude: 31.953790, Longitude: 35.299136 },
            { PlaceName: 'Corinth', Latitude: 37.905957, Longitude: 22.877882 },
            { PlaceName: 'Rhodes', Latitude: 36.441926, Longitude: 28.226722 },
            { PlaceName: 'Phenicia', Latitude: 33.563167, Longitude: 35.366346 },
            { PlaceName: 'Tarshish', Latitude: 36.952995, Longitude: -6.37934 },
            { PlaceName: 'Capernaum', Latitude: 32.880594, Longitude: 35.575158 },
            { PlaceName: 'Sea of Galilee', Latitude: 32.772757, Longitude: 35.591618 },
            { PlaceName: 'Dead Sea', Latitude: 31.511708, Longitude: 35.478388 },
            { PlaceName: 'Philippi', Latitude: 41.011959, Longitude: 24.286191 },
            { PlaceName: 'Thessalonica', Latitude: 40.632156, Longitude: 22.932087 },
            { PlaceName: 'Rome', Latitude: 41.900000, Longitude: 12.483333 },
            { PlaceName: 'Nineveh', Latitude: 36.359410, Longitude: 43.152887 },
            { PlaceName: 'Ur', Latitude: 30.962052, Longitude: 46.103742 },
            { PlaceName: 'Caesarea', Latitude: 32.499545, Longitude: 34.892185 },
            { PlaceName: 'Shur', Latitude: 30.235400, Longitude: 33.247000 },
            { PlaceName: 'Etham', Latitude: 30.467511, Longitude: 32.282767 },
            { PlaceName: 'Lycaonia', Latitude: 37.883530, Longitude: 32.494263 },
            { PlaceName: 'Paphos', Latitude: 34.754106, Longitude: 32.400162 },
            { PlaceName: 'Iconium', Latitude: 37.883530, Longitude: 32.494263 },
            { PlaceName: 'Perga', Latitude: 37.005208, Longitude: 30.904946 },
            { PlaceName: 'Cappadocia', Latitude: 36.731904, Longitude: 35.486302 }
        ];
        var cityjson = JSON.stringify(cityobj);
        var cities = JSON.parse(cityjson);
        cities.forEach(parseCityItem);
    };
    var parseCityItem = function (val, i) {
        var img = qualurl("Content/images/blackmarker.png");
        place = val.PlaceName;
        lat = parseFloat(val.Latitude);
        lon = parseFloat(val.Longitude);
        if (Inclusion(lat, lon)) {
            var marker = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'))
            });
            marker.setStyle(new ol.style.Style({
                image: new ol.style.Icon({
                    anchor: [8, 12],
                    anchorYUnits: 'pixels',
                    anchorXUnits: 'pixels',
                    opacity: 1.0,
                    src: img
                }),
                text: new ol.style.Text({
                    text: place,
                    font: 'bold 11pt Arial',
                    fill: new ol.style.Fill({ color: '#3c3c3c' }),
                    stroke: new ol.style.Stroke({ color: '#ffffff', width: 1 }),
                    textAlign: 'center',
                    textBaseline: 'top',
                    offsetX: parseInt(0, 0),
                    offsetY: parseInt(-26, 0),
                    rotation: 0,
                    placement: 'point',
                    maxAngle: 0.7853981633974483
                })
            }));
            var vectorSource = new ol.source.Vector({
                features: [marker]
            });
            markerVectorLayer = new ol.layer.Vector({
                source: vectorSource
            });
            markerVectorLayer.setZIndex(20);
            ol.extent.extend(extent, markerVectorLayer.getSource().getExtent());
            map.addLayer(markerVectorLayer);
        }
    };
    var getDistance = function (lat1, lon1, lat2, lon2) {
        var p = 0.017453292519943295;
        var c = Math.cos;
        var a = 0.5 - c((lat2 - lat1) * p) / 2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
        return 12742 * Math.asin(Math.sqrt(a)); // kilometers
    };
    var qualurl = function (pathinfo) {
        var qualifiedUrl;
        var pathArray = window.location.href.split('/');
        var protocol = pathArray[0];
        var host = pathArray[2];
        var site = pathArray[3];
        if (host.indexOf('localhost:') > -1) {
            qualifiedUrl = protocol + '//' + host + '/' + pathinfo;
        }
        else if (host.indexOf('localhost') > -1 || host.indexOf('192.168') > -1) {
            qualifiedUrl = protocol + '//' + host + '/' + site + '/' + pathinfo;
        }
        else {
            qualifiedUrl = protocol + '//' + host + '/' + pathinfo;
        }
        return qualifiedUrl;
    };
    var objSize = function (obj) {
        var count = 0;
        if (typeof obj === "object") {
            if (Object.keys) {
                count = Object.keys(obj).length;
            } else if (window._) {
                count = _.keys(obj).length;
            } else if (window.$) {
                count = $.map(obj, function () { return 1; }).length;
            } else {
                for (var key in obj) if (obj.hasOwnProperty(key)) count++;
            }
        }
        return count;
    };
}());