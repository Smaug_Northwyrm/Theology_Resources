﻿(function () {
    $(document).ready(function () {
        breakout();
        ipadrotation();
        var submitAutocomplete = function (event, ui) {
            var $input = $(this);
            $input.val(ui.item.label);
            showProgress();
            $("#searchbutton").trigger('click');
        };
        $("#word").autocomplete({select: submitAutocomplete, source: function (request, response) {
                if (request.term.length > 1) {
                    $.ajax({
                        url: qualifyUrl("AutoComplete?term=" + request.term),
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) { response(data); },
                        error: function (request, status, error) {
                            $(".wordofday").html(request.responseText);
                        }
                    });
                }
            } });
        $("#word").keypress(function (e) {
            if (e.which === 13) {
                e.preventDefault();
                $(this).blur();
                $('#searchbutton').focus().click();
            }
        });
        $(window).unload(function () {
            hideProgress();
            return true;
        });
        $("#searchbutton").click(function () {
            showProgress();
            var searchwordval = $("#word").val().toLowerCase() + '';
            var url = qualifyUrl("Dictionary/" + parseVerseRef(searchwordval));
            if (searchwordval === 'con' || searchwordval === 'bin' || searchwordval === 'nul') {
                url = qualifyUrl("Dictionary?word=" + searchwordval);
            }
            window.open(url, "_self");
            return false;
        });
        $(".bible").on("click", function () {
            showProgress();
            var linkref = $(this).attr("target");
            var dataword = $(this).attr("data-word");
            var url = qualifyUrl("BibleVerse/" + linkref);
            if (!dataword) { dataword = $("#WordHeading").html(); }
            if (dataword) { url = url + '/' + dataword; }
            window.open(url, "_self");
            return false;
        });
        $(".bible").on("mouseenter", function (event) { Showinfotip(event); });
        $(".bible").on("mousemove", function (event) { Positioninfotip(event); });
        $(".bible").on("mouseleave", function () { Hideinfotip(); });
        $(".crossref").click(function () {
            showProgress();
            var linkref = $(this).attr("target");
            if (linkref) {
                var wordval = linkref.toLowerCase() + '';
                var url = qualifyUrl("Dictionary/" + parseVerseRef(wordval));
                if (wordval === 'con' || wordval === 'bin' || wordval === 'nul') {
                    url = qualifyUrl("Dictionary?word=" + wordval);
                }
                window.open(url, "_self");
            //    $("#word").val(linkref);
            //    $("#searchbutton").focus().trigger('click');
            }
            return false;
        });
        $(".strongslink").click(function () {
            showProgress();
            var url = $(this).attr("href");
            if (url === null || url === '' || url === '#') {
                var linkref = $(this).attr("target");
                var emptystr = ' ';
                url = qualifyUrl('StrongsNo/' + linkref + '/' + emptystr);
            }
            window.open(url, "_self");
            return false;
        });
        $(".strongslink").on("mouseenter", function (event) { ShowStrongsinfotip(event); });
        $(".strongslink").on("mousemove", function (event) { Positioninfotip(event); });
        $(".strongslink").on("mouseleave", function () { Hideinfotip(); });
        $(".aniload").click(function () {
            showProgress();
            return true;
        });
        $("#word").val('');
        $("#word").focus();
    });
    var breakout = function () {
        if (top.location !== location) {
            top.location.href = document.location.href;
        }
    };
    var ipadrotation = function () {
        // Force rotation for iPad/iPhone
        if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
            var viewportmeta = document.querySelector('meta[name="viewport"]');
            if (viewportmeta) {
                viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
                if (document.addEventListener) {
                    document.addEventListener('gesturestart', function () {
                        viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=10';
                    }, false);
                }
            }
        }
    };
    var qualifyUrl = function (pathinfo) {
        var qualifiedUrl;
        var pathArray = window.location.href.split('/');
        var protocol = pathArray[0];
        var host = pathArray[2];
        var site = pathArray[3];
        if (host.indexOf('localhost:') > -1) {
            qualifiedUrl = protocol + '//' + host + '/' + pathinfo;
        }
        else if (host.indexOf('localhost') > -1 || host.indexOf('192.168') > -1) {
            qualifiedUrl = protocol + '//' + host + '/' + site + '/' + pathinfo;
        }
        else {
            qualifiedUrl = protocol + '//' + host + '/' + pathinfo;
        }
        return qualifiedUrl;
    };
    var showProgress = function () {
        var spinner = $("div#spinner");
        if (spinner.css('display') === 'none') {
            //spinner.html('');
            //spinner.append("<img src='" + qualifyUrl("Content/images/kjvdictani.gif") + "' /><p>Loading...</p>");
            spinner.fadeIn("fast");
        }
    };
    var hideProgress = function () {
        var spinner = $("div#spinner");
        if (spinner.css('display') !== 'none') {
            spinner.stop();
            spinner.fadeOut("fast");
        }
    };
    var Showinfotip = function (event) {
        $(".infotip").stop(true, true).remove();
        var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if (viewportWidth > 1023) {
            var ref = $(event.target).attr("target");
            if (ref.length) {
                $.ajax({
                    url: qualifyUrl("KjvVerse/" + ref),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('<div class="infotip">' + data + '</div>').appendTo('body');
                        $("div.infotip").css('display', 'none');
                        Positioninfotip(event);
                        $("div.infotip").delay(200).fadeTo(300, 1);
                    }
                });
            }
        }
    };
    var ShowStrongsinfotip = function (event) {
        $(".infotip").stop(true, true).remove();
        var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if (viewportWidth > 1023) {
            var strongsno = $(event.target).attr("target");
            if (strongsno.length) {
                $.ajax({
                    url: qualifyUrl("StrongsLink/" + strongsno),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('<div class="infotip">' + data + '</div>').appendTo('body');
                        $("div.infotip").css('display', 'none');
                        Positioninfotip(event);
                        $("div.infotip").delay(200).fadeTo(300, 1);
                    }
                });
            }
        }
    };
    var Hideinfotip = function () {
        $(".infotip").stop(true, true).remove();
    };
    var Positioninfotip = function (event) {
        var scroll = $(document).scrollTop();
        var infotipH = $(".infotip").height();
        var infotipX = event.pageX + 15;
        var infotipY = event.pageY - infotipH - 20;
        var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        if (infotipX + 260 > viewportWidth) { infotipX = viewportWidth - 275; }
        if (infotipY < scroll + 1) { infotipY = event.pageY + 16; }
        $('.infotip').css({ top: infotipY, left: infotipX });
        $(this).bind({ mouseleave: Hideinfotip });
    };
    var parseVerseRef = function (searchwordval) {
        searchwordval = $.trim(searchwordval);
        if (searchwordval.indexOf('.') > 0 || searchwordval.indexOf(':') > 0) {
            if (searchwordval.indexOf(':') > 0) {
                searchwordval = searchwordval.replace(':', '$');
                if (searchwordval.indexOf(' ') < 0) {
                    searchwordval = searchwordval.replace('.', ' ');
                }
                else {
                    searchwordval = searchwordval.replace('.', '');
                }
            } else if (searchwordval.indexOf(' ') < 0) {
                searchwordval = searchwordval.replace('.', ' ');
                searchwordval = searchwordval.replace('.', '$');
            } else {
                searchwordval = searchwordval.replace('.', '$');
                if (searchwordval.indexOf('.') > 0) {
                    searchwordval = searchwordval.replace('$', '');
                    searchwordval = searchwordval.replace('.', '$');
                }
            }
            searchwordval = searchwordval.replace(' ', '_');
            return searchwordval;
        }
        else {
            return encodeURIComponent(searchwordval);
        }
    };
}());
